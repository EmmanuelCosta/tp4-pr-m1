package fr.upem.tcp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 10/02/14
 * Time: 11:43
 * To change this template use File | Settings | File Templates.
 */
public class TCPUpperCaseServer {

    private final ServerSocket serverSocket;
    private final Charset charset;
    private final Semaphore semaphore;



    public TCPUpperCaseServer(int listeningPort, String charsetName) throws IOException {
        this.serverSocket = new ServerSocket(listeningPort);
        this.charset = Charset.forName(charsetName);
        this.semaphore = new Semaphore(1) ;
    }

    public TCPUpperCaseServer(int listeningPort, String charsetName,int maxSize) throws IOException {
        this.serverSocket = new ServerSocket(listeningPort);
        this.charset = Charset.forName(charsetName);
        this.semaphore = new Semaphore(maxSize) ;
    }

    void serve(Socket client) {

        try {
            client.setSoTimeout(60000);
            //For Reading
            InputStream inputStream = client.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, charset));

            //For writing
            OutputStream outputStream = client.getOutputStream();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream));
            while (true) {
                String received = br.readLine();
                if(received == null){
                    break;
                }

                System.out.println("Server received :" + received);
                String send = received.toUpperCase();

                bw.write(send);
                bw.newLine();
                bw.flush();

                if (received.endsWith(".")) {

                    break;
                }


            }
        } catch (SocketTimeoutException e) {
            System.err.println("socket time out ");

        } catch (IOException e) {
               e.printStackTrace();
        }  finally{
            semaphore.release();
            silentlyClose(client);
        }

    }
           //count down lauch method java

    private void silentlyClose(Socket client) {
        try {
            System.out.println("socket close silently");
            client.close();
        } catch (IOException e) {

        }
    }

    public void launchConcurrentOnDemand( ) {

        while (!Thread.interrupted()) {
            // if accept() throws an exception, propagate it: server's down

            try {
                Socket client = serverSocket.accept();
                System.out.println("New client accepted : " + client.getRemoteSocketAddress());
                new Thread(() -> {

                    // serve this client...

                    serve(client);
                }).start();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
    public void launchConcurrentOnDemandBounded( ) {


        while (!Thread.interrupted()) {
            // if accept() throws an exception, propagate it: server's down
            System.out.println("===> " + semaphore.availablePermits());
            try {

                semaphore.acquire();
                Socket client = serverSocket.accept();
                System.out.println("New client accepted : " + client.getRemoteSocketAddress());

                new Thread(() -> {


                        serve(client);

                }).start();


            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    public void launchIterative() throws IOException {
        while (!Thread.interrupted()) {
            // if accept() throws an exception, propagate it: server's down
            Socket client = serverSocket.accept();
            System.out.println("New client accepted : " + client.getRemoteSocketAddress());
            // serve this client...


            serve(client);

        }
    }

    public void shutDown(){

    }
    /**
     * Si deux clients tentent d'acceder simultanement
     * le premier client sera servi alors que le second est mis en attente
     * une fois la connexion du premier terminée
     * le second est automatquement pris en charge
     */

    public static void main(String[] args) throws IOException {
        int maxClient = Integer.parseInt(args[0]);
        int listeningPort = Integer.parseInt(args[1]);
        TCPUpperCaseServer server = new TCPUpperCaseServer(listeningPort, args[2],maxClient);
        //server.launchIterative();
        //server.launchConcurrentOnDemand();
        server.launchConcurrentOnDemandBounded();
    }
}

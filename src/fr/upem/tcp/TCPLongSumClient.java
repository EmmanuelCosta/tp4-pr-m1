package fr.upem.tcp;

import utils.Converter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 10/02/14
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
public class TCPLongSumClient {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket(args[0], Integer.valueOf(args[1]));

        //For Reading
        InputStream inputStream = socket.getInputStream();

        //For writing
        OutputStream outputStream = socket.getOutputStream();

        while(true){
            byte [] buff1 = new byte[8];


            System.out.println("Please enter long to sum :");
            Scanner scanner = new Scanner(System.in);
            long l = scanner.nextLong();
            Converter.longToByteArray(l, buff1);
            outputStream.write(buff1);



            if(l == 0){
                socket.shutdownOutput();



                break;
            }
        }

        byte [] buff2 = new byte[8];
        byte [] recup = new byte[8];

        int offset=8;
        int i =0;
        int read=0;
        int sum=0;

        do{
            read = inputStream.read(recup);
            if(read == -1){
                System.out.println("Server do not respect contract ");
                break;
            }
            System.out.println("retreive sum ... "+read);
            offset = offset-read;
            System.arraycopy(recup, 0, buff2, i, read);
            i+=read;
            sum = read;

        }while(offset !=0) ;



        System.out.println(sum);

        System.out.println(" Sum received = "+Converter.byteArrayToLong(buff2));

        socket.close();


    }
}

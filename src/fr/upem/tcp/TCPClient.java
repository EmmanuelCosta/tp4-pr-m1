package fr.upem.tcp;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 10/02/14
 * Time: 11:21
 * To change this template use File | Settings | File Templates.
 */
public class TCPClient {
    //TODO AMELIORATE ON USING ONE NEW THREAD
    public static void main(String[] args) throws IOException, InterruptedException {
        Socket socket = new Socket(args[0], Integer.valueOf(args[1]));
        String codage = args[2];

        //For Reading
        InputStream inputStream = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, codage));

        //For writing
        OutputStream outputStream = socket.getOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream));

        Thread writer = new Thread(() -> {
            while (true) {


                try {
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("Write :");
                    String line = scanner.nextLine();

                    bw.write(line);
                    bw.newLine();
                    bw.flush();

                    if (line.endsWith(".")) {
                        System.out.println("writing  a . at the end of my line \n so i shutdown the writing chanel");
                        socket.shutdownOutput();

                        break;

                    }

                } catch (IOException e) {
                    System.err.println("something when wrong while writing in the socketOutputstream");
                }

            }

        });
        writer.setDaemon(true);
        writer.start();

        while (true) {
            try {
                String line = br.readLine();
                System.out.println(" Read : " + line);

                if (line == null || line == "-1" || line.endsWith(".")) {
                    System.out.println("reading " + line + "  so i quit");
                    socket.shutdownInput();
                    break;
                }


            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }


    }
}

package fr.upem.tcp;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 10/02/14
 * Time: 11:03
 * To change this template use File | Settings | File Templates.
 */
public class SimpleTCPClient {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket(args[0], Integer.valueOf(args[1]));
        String codage=args[2];

        //For Reading
        InputStream inputStream = socket.getInputStream();
        BufferedReader br= new BufferedReader(new InputStreamReader(inputStream,codage));

        //For writing
        OutputStream outputStream = socket.getOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream)) ;


        while(true){
            Scanner scanner = new Scanner(System.in);
            System.out.println("Write :");
            String s = scanner.nextLine();

            bw.write(s);
            bw.newLine();
            bw.flush();

            String line = br.readLine();

            System.out.println(" Read : "+line);
        }
    }
}
